const express = require("express");
const fileUpload = require("express-fileupload");
const app = express();

const Usuario = require("../models/usuario");
const Producto = require("../models/producto");

const fs = require("fs");
const path = require("path");

// default options
// Para que lleguen archivos por el request
app.use(fileUpload());

app.put("/upload/:tipo/:id", function (req, res) {
  let tipo = req.params.tipo;
  let id = req.params.id;

  // Validar que lleguen archivos a la ruta
  if (!req.files || Object.keys(req.files).length === 0) {
    return res.status(400).json({
      ok: false,
      err: {
        message: "No se ha seleccionado ningun archivo",
      },
    });
  }

  // Validar tipo

  let tiposValido = ["productos", "usuarios"];
  if (tiposValido.indexOf(tipo) < 0) {
    return res.status(400).json({
      ok: false,
      err: {
        message: "Las tipos permitidos son " + tiposValido.join(", "),
        tipo: tipo,
      },
    });
  }

  //   Obtener el archivo
  let archivo = req.files.archivo;
  let nombreCortado = archivo.name.split(".");
  let extension = nombreCortado[nombreCortado.length - 1];

  //   Validar extensiones permitidas

  let extensionesValidas = ["png", "jpg", "gif", "jpeg"];

  if (extensionesValidas.indexOf(extension) < 0) {
    return res.status(400).json({
      ok: false,
      err: {
        message:
          "Las extensiones permitidas son " + extensionesValidas.join(", "),
        ext: extension,
      },
    });
  }

  //   Cambiar nombre del archivo
  let nombreArchivo = `${id}-${new Date().getMilliseconds()}.${extension}`;

  //  Guardar la imagen en la carpeta uploads
  archivo.mv(`uploads/${tipo}/${nombreArchivo}`, (err) => {
    if (err)
      return res.status(500).json({
        ok: false,
        err,
      });

    //   Aqui imagen cargada
    tipo === "usuarios"
      ? imagenUsuario(id, res, nombreArchivo)
      : imagenProducto(id, res, nombreArchivo);
  });
});

function imagenUsuario(id, res, nombreArchivo) {
  Usuario.findById(id, (err, productoDB) => {
    if (err) {
      borraArchivo(nombreArchivo, "usuarios");
      return res.status(500).json({
        ok: false,
        err,
      });
    }

    if (!productoDB) {
      borraArchivo(nombreArchivo, "usuarios");
      return res.status(400).json({
        ok: false,
        err: {
          message: "Usuario no existe",
        },
      });
    }

    borraArchivo(productoDB.img, "usuarios");

    productoDB.img = nombreArchivo;

    productoDB.save((err, usuarioGuardado) => {
      res.json({
        ok: true,
        usuario: usuarioGuardado,
        img: nombreArchivo,
      });
    });
  });
}

function imagenProducto(id, res, nombreArchivo) {
  Producto.findById(id, (err, productoDB) => {
    if (err) {
      borraArchivo(nombreArchivo, "productos");
      return res.status(500).json({
        ok: false,
        err,
      });
    }

    if (!productoDB) {
      borraArchivo(nombreArchivo, "productos");
      return res.status(400).json({
        ok: false,
        err: {
          message: "Usuario no existe",
        },
      });
    }

    borraArchivo(productoDB.img, "productos");

    productoDB.img = nombreArchivo;

    productoDB.save((err, productoGuardado) => {
      res.json({
        ok: true,
        producto: productoGuardado,
        img: nombreArchivo,
      });
    });
  });
}

function borraArchivo(nombreImg, tipo) {
  // Busca la imagen de la base de datos
  let pathImg = path.resolve(__dirname, `../../uploads/${tipo}/${nombreImg}`);

  // Comprueba que exista la img anterior para borrarla
  if (fs.existsSync(pathImg)) {
    fs.unlinkSync(pathImg);
  }
}

module.exports = app;
